RoActDat is a plugin (or app) that will block sony (or anyone else) from deactivating your PSVita.

APP:
Just run the app and tm0 will be remounted Read/Only (BUT ONLY UNTIL NEXT REBOOT)

Plugin:
Add RoActDat.skprx to *KERNEL in config.txt, to make tm0: be remounted as read-only whenever henkaku loads
(Which if you use enso is at bootup)

To test if its working just try to copy a file to tm0 it should throw error 0x8001001E in vitashell.

!Works on 3.65 and 3.68!

APP: https://bitbucket.org/SilicaAndPina/roactdat/downloads/RoActDat.vpk  
KPLUGIN: https://bitbucket.org/SilicaAndPina/roactdat/downloads/RoActDat.skprx  